angular.module('bar24', ['ionic', 'bar24.controllers', 'bar24.services', "firebase"])

  /* TODO * /

  . Data doorsturen naar database
  . Stappen bijhouden
  . Styling
  . Classes meegeven on active
  . // LocalStorage voor naam en mood, of juist niet?
  . // View transition bij button klik
  . // Welkom terug, bij terugkomst
  . // Popup
  . // Afhandeling meerdere personen
  . // Notificatie sturen

  /**/

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  $stateProvider

  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  .state('tab.welcome', {
    url: '/welcome',
    cache: false,
    views: {
      'tab-welcome': {
        templateUrl: 'templates/tab-welcome.html',
        controller: 'WelcomeCtrl'
      }
    }
  })

  .state('tab.mood', {
    url: '/mood',
    cache: false,
    views: {
      'tab-mood': {
        templateUrl: 'templates/tab-mood.html',
        controller: 'MoodCtrl'
      }
    }
  })

  .state('tab.drinks', {
      url: '/drinks',
      cache: false,
      views: {
        'tab-drinks': {
          templateUrl: 'templates/tab-drinks.html',
          controller: 'DrinksCtrl'
        }
      }
    })
    .state('tab.drinks-detail', {
      url: '/drinks/:drinksId',
      cache: false,
      views: {
        'tab-drinks': {
          templateUrl: 'templates/drinks-detail.html',
          controller: 'DrinksDetailCtrl'
        }
      }
    })
    .state('tab.ordered', {
      url: '/ordered',
      views: {
        'tab-drinks': {
          templateUrl: 'templates/ordered.html',
          controller: 'OrderedCtrl'
        }
      }
    });

  //default scherm instellen op "Welcome"
  $urlRouterProvider.otherwise('/tab/welcome');

});
