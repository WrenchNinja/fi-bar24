angular.module('bar24.services', [])
.factory('Drinks', function() {
  
  // Voeg hier de drank-type objecten toe, waarin weer alle mogelijke dranken zitten.

  var drinks = [
  {
    id: 0,
    name: 'Coffee\'s & Tea\'s',
    singularName: 'coffee or tea',
    logo: 'img/logo_coffee.png',
    assortment: [{
      id: 'espresso',
      image: 'content/Coffee_Espresso.png',
      name: "Espresso",
      description: "Espresso is coffee brewed by forcing a small amount of nearly boiling water under pressure through finely ground coffee beans. Espresso is generally thicker than coffee brewed by other methods, has a higher concentration of suspended and dissolved solids, and has crema on top."
    },{
      id: 'decaf',
      image: 'content/Coffee_Decaf.png',
      name: "Decaf",
      description: "A decaffeinated cup of coffee. A direct decaffeination process involves the use of carbon dioxide as a solvent. The coffee beans are soaked in compressed CO2, which removes 97 percent of the caffeine. The solvent containing the extracted caffeine evaporates when the beans return to room temperature."
    },{
      id: 'cappuccino',
      image: 'content/Coffee_Cappuccino.png',
      name: "Cappuccino",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat dolor explicabo fuga, animi ipsam ad tempore minus aliquam quae quod est veritatis sint eaque eos iure rem distinctio, quo, nam."
    },{
      id: 'latte-macchiato',
      image: 'content/Coffee_Lattemacchiato.png',
      name: "Latte Macchiato",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perspiciatis eos laboriosam beatae dolorem consectetur eius dicta, voluptates fugit nam quo deserunt rerum, accusantium neque, rem inventore quidem aspernatur unde dolore?"
    },{
      id: 'monin-liquer',
      image: 'content/Coffee_Monin.png',
      name: "Monin Liqueur",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Odio facere, deleniti, laborum rerum, velit provident placeat enim atque doloribus esse, ipsum excepturi maiores? Quibusdam nihil itaque suscipit, quaerat quis qui."
    },{
      id: 'tea',
      image: 'content/Coffee_Tea.png',
      name: "Tea",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus ad sint nobis explicabo dignissimos et qui, atque fugit nisi quaerat obcaecati provident sed! Maxime eaque natus, tempore cum in iusto."
    }]
  },{
    id: 1,
    name: 'Soft Drinks',
    singularName: 'soft drink',
    logo: 'img/logo_softdrinks.png',
    assortment: [{
      id: 'cola',
      image: 'content/Soft_Cola.png',
      name: "Coca Cola",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Recusandae veniam dolore mollitia fugiat praesentium excepturi quas. Cumque est odit eius rerum soluta. Dolores voluptatum quaerat maxime enim, laudantium cum soluta?"
    },{
      id: 'cola-light',
      image: 'content/Soft_Light.png',
      name: "Cola Light",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Expedita dolor doloribus facilis incidunt eligendi error nobis vitae omnis, alias tenetur, cumque sed recusandae! Earum dolor, cumque molestias voluptas repellendus nemo."
    },{
      id: 'icetea',
      image: 'content/Soft_Icetea.png',
      name: "Iced Tea",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime repellendus ullam laborum aperiam, reiciendis quis quasi eveniet repellat incidunt alias itaque similique illo, consectetur magni nesciunt, minima vitae non. Enim!"
    },{
      id: 'spa-rood',
      image: 'content/Soft_Rood.png',
      name: "Sparkling Water",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem voluptatem sit deleniti, iusto autem, aliquam inventore expedita, ipsa culpa, omnis non dicta dolore sequi nulla distinctio ad nisi maiores velit."
    },{
      id: 'ice-tea-green',
      image: 'content/Soft_Iceteagreen.png',
      name: "Iced Tea Green",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Soluta libero sapiente fuga itaque, excepturi deleniti in ratione blanditiis enim vitae provident accusantium temporibus voluptas, saepe reiciendis ad asperiores ipsum, a."
    },{
      id: 'water',
      image: 'content/Soft_Water.png',
      name: "Water",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero laborum eum officiis itaque, saepe fugit ut sint delectus cumque facere sed dolorum consequuntur, voluptate assumenda dolorem reprehenderit modi necessitatibus. Soluta."
    }]
  }, {
    id: 2,
    name: 'Wines',
    singularName: 'wine',
    logo: 'img/logo_wines.png',
    assortment: [{
      id: 'outoftheblue',
      image: 'content/Wines_Outoftheblue.png',
      name: "Out of the Blue",
      description: "Klassieke Provence Rosé. En een zorgvuldige selectie van de Grenache en de Cinsault, zorgen voor een raszuivere Provence rosé met veel finesse. In de geur veel expressie en tonen van kersen, bessen en iets van gebakjes. Elegant en fruitig van smaak met een mooie zalvende afdronk."
    },{
      id: 'goldmountains',
      image: 'content/Wines_Cheninnoir.png',
      name: "Chenin Noir",
      description: "Frisse, sappige wijn met accent op fruit."
    },{
      id: 'brindisiriserva',
      image: 'content/Wines_Brindisiriserva.png',
      name: "Brindisi Riserva",
      description: "Een blend van Puglia's mooiste. Primitivo, Negro Amaro en Nero di Troia. Zacht zondoorstoofd fruit en aroma's van pruimen. Lichte houtrijping geeft de wijn een nobele smaak."
    },{
      id: 'highaltitude',
      image: 'content/Wines_Voignier.png',
      name: "Chardonnay Voignier",
      description: "Gele kleur met groene tinten. Frisse wijn met aroma’s van appel, peer en citrusvruchten. In de smaak weer fris, lichtzoet, lang aanhoudende afdronk."
    },{
      id: 'extremechardonnay',
      image: 'content/Wines_Extreme.png',
      name: "Extreme Chardonnay",
      description: "Extreme!"
    },{
      id: 'pinotnoir',
      image: 'content/Wines_Pinotnoir.png',
      name: "Pinot Noir",
      description: "Elegante en verfijnde wijn met licht kersenfruit. Houttonen zijn mooi verweven. Leuk alternatief voor de generieke Bourgogne Pinot Noir. Deze wijn heeft een paar maanden gerijpt op eikenhout."
    }]
  }, {
    id: 3,
    name: 'Beers',
    singularName: 'beer',
    logo: 'img/logo_beers.png',
    assortment: [{
      id: 'grolsch',
      image: 'content/Beer_Grolshswingtop.png',
      name: "Grolsch Wing Top",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe facilis distinctio enim at, similique esse rem iusto reiciendis mollitia officia beatae rerum fugit, odit qui veritatis repellendus necessitatibus neque architecto!"
    },{
      id: 'grolsh_crown',
      image: 'content/Beer_kroonkurk.png',
      name: "Grolsh Crowncork",
      description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cumque nulla similique assumenda, vel quasi ipsa, eveniet necessitatibus neque tempore, cupiditate voluptate. Quas ea maxime a minima, explicabo eum non harum!"
    }]
  },{
    id: 4,
    name: "Gin & Tonic",
    singularName: 'gin & tonic',
    logo: 'img/logo_gintonic.png',
    assortment: [{
      id: 'hendricks',
      image: 'content/2_Hendrick.png',
      name: "Hendrick's",
      description: "Quirky producer Hendricks make their pot-still distilled gin using cucumber as one of the primary botanicals. This makes for a unique, tasty and incredibly refreshing Gin."
    },{
      id: 'colombian',
      image: 'content/1_Colombian.png',
      name: "Colombian",
      description: "From the clever minds and talented hands that brought us Dictador Rum comes an aged Colombian in. Using a botanical selection based on the limon mandarino, a hybrid between a mandarin orange and a lemon, they have created a wonderful, citrus-forward gin and given it the rather regal name, Treasure. After distillation, it is aged in rum barrels for 35 weeks to enhance and round out the flavours."
    },{
      id: 'bulldog',
      image: 'content/1_Bulldog.png',
      name: "Bulldog",
      description: "A delicious English gin made with poppy, dragon eye, lotus leaves, citrus, almond, lavender and various other botanicals (12 in total). This is four times distilled in copper pot stills, and Bulldog had the highest rating ever received by a gin from Wine Enthusiast Magazine. It was also voted a Top 50 spirit!"
    },{
      id: 'copperhead',
      image: 'content/2_Copperhead.png',
      name: "Copperhead",
      description: "Copperhead Gin is a Belgian expression, made with a selection of five botanicals - Juniper, Cardamom, Orange peel, Angelica and Coriander. It was apparently named after Mr. Copperhead, an alchemist who was searching for the elixir of life. Along the way, he produced the recipe for this refreshing gin!"
    },{
      id: 'monkey',
      image: 'content/3_Monkey.png',
      name: "Monkey 47",
      description: "An unusual gin from the Black Forest in Germany, Monkey 47 contains a unique ingredient. No, not that! Cranberries! The 47 comes from the number of botanicals that go into this unique gin, and the fact it's bottled at a healthy 47%."
    },{
      id: 'bombay',
      image: 'content/1_Bombay.png',
      name: "Bombay Sapphire",
      description: "\"A twist on a classic\" is an overused phrase, but is surely appropriate here. This edition of one of the most popular London Dry Gins in the world uses the ten classic Bombay Sapphire botanicals plus lemongrass and Vietnamese black peppercorns."
    },{
      id: 'nordes',
      image: 'content/3_Nordes.png',
      name: "Nordes",
      description: "Nordés Atlantic Galician Gin is a rather intriguing Spanish gin made with an Albariño-grape-based spirit, rather than your more commonly seen grain-based spirit, along with 12 botanicals including lemon peels, hibiscus, liquorice and eucalyptus. The result is a bold, floral flavour profile that would befit summer drinks and cocktails. The bottle is quite stylish too - very impressive stuff!"
    },{
      id: 'bobby',
      image: 'content/1_Bobby.png',
      name: "Bobby's",
      description: "A Dutch gin which was launched in 2014, Bobby's Schiedam Dry Gin features a recipe inspired by the creator's grandfather, Bobby Alfons. It's made with a combination of local and exotic ingredients, including juniper, clove, lemongrass, cubeb peppers and rosehips. Highlighting the meeting of Dutch and Indonesian flavours in the spirit, Bobby's Schiedam Dry Gin comes in a bottle which harks back to the style of vessel used to hold jenever, with a pattern inspired by Indonesian designs."
    }]
  }
  ];

  return {
    all: function() {
      return drinks;
    },
    get: function(drinksId) {
      for (var i = 0; i < drinks.length; i++) {
        if (drinks[i].id === parseInt(drinksId)) {
          return drinks[i];
        }
      }
      return null;
    }
  };

});