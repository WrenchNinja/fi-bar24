angular.module('bar24.controllers', [])
	.controller('BeaconCtrl', function($scope, $rootScope, $ionicPlatform, $cordovaBeacon, $firebaseArray, $firebaseObject) {

		setInterval(function(){
			console.log('still here');
		}, 1000);

		var databaseRef = new Firebase("https://fi-vip.firebaseio.com/");
		var databaseObj = $firebaseObject(databaseRef);

		function doTimeout(){
		  //console.log('starting countdown', beaconActive, startedCountdown);

		  if(startedCountdown == false && beaconActive == true){
		    startedCountdown = true;
		    timer = setTimeout(function(){
		      $scope.currentMessage = "You aren't so close";
		      console.log("not close");

		      beaconStat.status = 0;
		      beaconStat.name = "";
		      beaconStat.color = "";
		      beaconStat.$save().then(function(beaconStatusRef){
		        beaconStatusRef.key() === beaconStat.$id;
		      }, function(error){
		        console.log("Error:", error);
		      });

		      beaconActive = false;
		      $scope.$apply();
		      startedCountdown = false;
		    }, 2500);
		  }

		}

		function stopTimeout(){
		  console.log('clearing countdown', beaconActive);
		  startedCountdown = false;
		  clearTimeout(timer);
		}

		var handleBeacons = function(beacon, range, accuracy){
			if(range == "ProximityNear" || range == "ProximityImmediate" || accuracy < 2.2 ){
			  stopTimeout();
			  if($("#color").val() == "purple"){
			    console.log($("#color").val());
			    $("ion-header-bar").removeClass('green red blue yellow orange');
			    $("ion-header-bar").addClass('purple');
			  } else if($("#color").val() == "green" ){
			    console.log($("#color").val());
			    $("ion-header-bar").removeClass('purple red blue yellow orange');
			    $("ion-header-bar").addClass('green');
			  } else if($("#color").val() == "red" ){
			    console.log($("#color").val());
			    $("ion-header-bar").removeClass('green purple blue yellow orange');
			    $("ion-header-bar").addClass('red');
			  } else if($("#color").val() == "blue"){
			    console.log($("#color").val());
			    $("ion-header-bar").removeClass('green red purple yellow orange');
			    $("ion-header-bar").addClass('blue');
			  } else if($("#color").val() == "yellow"){
			    console.log($("#color").val());
			    $("ion-header-bar").removeClass('green red blue purple orange');
			    $("ion-header-bar").addClass('yellow');
			  } else if($("#color").val() == "orange"){
			    console.log($("#color").val());
			    $("ion-header-bar").removeClass('green red blue yellow purple');
			    $("ion-header-bar").addClass('orange');
			  }
			  if(beaconActive == false){
			    beaconActive = true;
			    $scope.currentMessage = "You are close to the radio";

			    beaconStat.status = 1;
			    beaconStat.name = $("#username").val();
			    beaconStat.color = $( "#color" ).val();
			    beaconStat.$save().then(function(beaconStatusRef){
			      beaconStatusRef.key() === beaconStat.$id;
			    }, function(error){
			      console.log("Error:", error);
			    });

			    $scope.$apply();
			  }
			} else{
			  $("ion-header-bar").removeClass('green red blue yellow orange purple');
			  doTimeout();
			}
		}

		// Simulate Beacon ranges
		$(document).on("keypress", function(e){

		  if(e.keyCode == 49){
		    handleBeacons("Shoe", "ProximityNear");
		  } else if(e.keyCode == 50){
		    handleBeacons("Shoe", "ProximityFar");
		  } else if(e.keyCode == 51){
		    handleBeacons("Shoe", "ProximityUnknown");
		  }

		});

		/* Cordova * /
		$ionicPlatform.ready(function(){

		  $cordovaBeacon.requestWhenInUseAuthorization();

		  $rootScope.$on("$cordovaBeacon:didRangeBeaconsInRegion", function(event, data) {
		      var uniqueBeaconKey;

		      for(var i = 0; i < data.beacons.length; i++) {
		          uniqueBeaconKey = data.beacons[i].uuid + ":" + data.beacons[i].major + ":" + data.beacons[i].minor;
		          $scope.beacons[uniqueBeaconKey] = data.beacons[i];
		          var object = data;
		          $scope.log = data;

		          handleBeacons(data.beacons[i].uuid, data.beacons[i].proximity);
		          /* Beacon monitoring * /
		          if( data.beacons[i].uuid == "D0D3FA86-CA76-45EC-9BD9-6AF48832B1B5" && data.beacons[i].proximity == "ProximityNear" ){
		            $scope.currentMessage = "Bike is close";
		          }

		          if( data.beacons[i].uuid == "D0D3FA86-CA76-45EC-9BD9-6AF48832B1B5" && data.beacons[i].proximity == "ProximityImmediate" ){
		            $scope.currentMessage = "Bike is very close";
		          }

		          if( data.beacons[i].uuid == "D0D3FA86-CA76-45EC-9BD9-6AF46B25985B" && data.beacons[i].proximity == "ProximityNear" ){
		            $scope.currentMessage = "Bag";
		          }

		          if( data.beacons[i].proximity == "ProximityNear" || data.beacons[i].proximity == "ProximityImmediate"){
		            $scope.beaconActive = true;
		            stopTimeout();
		            $scope.currentMessage = "You are close to the radio";
		          } else if( data.beacons[i].proximity != "ProximityNear" || data.beacons[i].proximity != "ProximityImmediate" ){
		            if($scope.beaconActive = true){

		              doTimeout();
		              
		            }
		            
		          }

		          // master comment below 
		          /** /
		
		      }

		      $scope.$apply();

		  });

		  $cordovaBeacon.startRangingBeaconsInRegion($cordovaBeacon.createBeaconRegion("Ice", "B9407F30-F5F8-466E-AFF9-25556B57FE6D", 40260, 52233 ));

		})
	
	/**/

	})

	.controller('WelcomeCtrl', function($scope, $firebaseArray, $firebaseObject) {


		var connectionID = Math.round(Math.random(0,1) * 1000000);

		var databaseRef = new Firebase("https://fi-vip.firebaseio.com/");
		var databaseObj = $firebaseObject(databaseRef);

		databaseObj.$loaded()
		.then(function(data) {
			databaseObj.currentConnection = "id"+connectionID;
			databaseObj.screen.step = "Welcome";
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
			if( $(".fn-input").val() == "" ){
				var firstName = $('.fn-input').val();
				databaseObj.user.firstName = firstName;
				databaseObj.$save().then(function(databaseRef){
				  databaseRef.key() === databaseObj.$id;
				}, function(error){
				  console.log("Error:", error);
				});
			}
			if( $(".ln-input").val() == "" ){
				var lastName = $('.fn-input').val();
				databaseObj.user.lastName = lastName;
				databaseObj.$save().then(function(databaseRef){
				  databaseRef.key() === databaseObj.$id;
				}, function(error){
				  console.log("Error:", error);
				});
			}
		})
		.catch(function(error) {
			console.error("Error:", error);
		});

		$('.fn-input').on('change', function(){
			var firstName = $('.fn-input').val();
			databaseObj.user.firstName = firstName;
			databaseObj.screen.step = "Mood";
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
		});

		$('.ln-input').on('change', function(){
			var lastName = $('.ln-input').val();
			databaseObj.user.lastName = lastName;
			databaseObj.$save().then(function(databaseRef){
				databaseRef.key() === databaseObj.$id;
			}, function(error){
				console.log("Error:", error);
			});
		});

	})

	.controller('MoodCtrl', function($scope, $firebaseArray, $firebaseObject) {

		var databaseRef = new Firebase("https://fi-vip.firebaseio.com/");
		var databaseObj = $firebaseObject(databaseRef);

		setTimeout(function(){ // pure for chronological order
			databaseObj.screen.step = "Mood";
			databaseObj.mood = "";
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
		},0);

		$scope.setMood = function(mood){
			databaseObj.mood = mood;
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
		}


		
	})

	.controller('DrinksCtrl', function($scope, Drinks, $firebaseArray, $firebaseObject) {

		var databaseRef = new Firebase("https://fi-vip.firebaseio.com/");
		var databaseObj = $firebaseObject(databaseRef);

		$scope.drinks = Drinks.all();

		setTimeout(function(){ // pure for chronological order
			databaseObj.screen.step = "drinkSelection";
			databaseObj.drinks = "";
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
		},0);


	})

	.controller('DrinksDetailCtrl', function($scope, $stateParams, Drinks, $firebaseArray, $firebaseObject) {

		var databaseRef = new Firebase("https://fi-vip.firebaseio.com/");
		var databaseObj = $firebaseObject(databaseRef);
		var currentCategorie;

		$scope.drink = Drinks.get($stateParams.drinksId);
		$scope.assortment = $scope.drink.assortment;
		$scope.selectedItem = null;

		setTimeout(function(){ // pure for chronological order
			currentCategorie = $('.categorie').text();
			databaseObj.drinks = currentCategorie;
			databaseObj.screen.step = "drinkDetail";
			databaseObj.drinkInfo.name = "";
			databaseObj.drinkInfo.drinkID = "";
			databaseObj.drinkInfo.description = "";
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
		},0);

		$scope.drinkInfo = function($event){
			var name = $event.target.dataset.name;
			var id = $event.target.dataset.id;
			var description = $event.target.dataset.description;
			databaseObj.drinkInfo.name = name;
			databaseObj.drinkInfo.drinkID = id;
			databaseObj.drinkInfo.description = description;
			//databaseObj.drinkInfo.image = 
			databaseObj.$save().then(function(databaseRef){
			  databaseRef.key() === databaseObj.$id;
			}, function(error){
			  console.log("Error:", error);
			});
		}

		

	})

	.controller('OrderedCtrl', function($scope, $firebaseArray, $firebaseObject){

		var databaseRef = new Firebase("https://fi-vip.firebaseio.com/");
		var databaseObj = $firebaseObject(databaseRef);

		databaseObj.$loaded()
		.then(function(data) {
			$scope.orderedDrink = databaseObj.drinkInfo.name;
		})
		.catch(function(error) {
			console.error("Error:", error);
		});

		setTimeout(function(){
			databaseObj.screen.step = "ordered";
			databaseObj.$save().then(function(databaseRef){
				databaseRef.key() === databaseObj.$id;
			}, function(error){
				console.log("Error:", error);
			});
		},0);
	});